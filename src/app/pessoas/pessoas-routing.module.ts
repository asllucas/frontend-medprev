import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PessoasFormComponent } from "./containers/pessoas-form/pessoas-form.component";
import { PessoasListagemComponent } from "./containers/pessoas-listagem/pessoas-listagem.component";

const routes: Routes = [

    {
        path: '',
        children: [
            {
                path: '',
                redirectTo: 'pessoas'
            },
            {
                path: 'pessoas',
                component: PessoasListagemComponent,

            },
            {
                path: 'cadastro',
                component: PessoasFormComponent
            },
            {
                path: ':id/alterar',
                component: PessoasFormComponent
            }
        ]
    }

]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PessoasRoutingModule{

}