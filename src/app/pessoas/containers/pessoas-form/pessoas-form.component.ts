import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PessoasApi } from '../../api/pessoas.api';
import { cepMask, cnpjMask, cpfMask, foneCelMask, foneFixMask } from '../../models/mask';
import { NotificacaoService } from '../../services/notificacao.service';
import * as moment from 'moment';



@Component({
  selector: 'app-pessoas-form',
  templateUrl: './pessoas-form.component.html',
  styleUrls: ['./pessoas-form.component.scss']
})
export class PessoasFormComponent implements OnInit {

  pessoaForm: FormGroup;
  cpfMask = cpfMask;
  cnpjMask = cnpjMask;
  cepMask = cepMask;
  foneCelMask = foneCelMask;
  foneFixMask = foneFixMask;
  isPessoaFisica = true;
  isPessoaJuridica = false;
  formSubmitted = false;
  id: any;

  
  constructor(private form: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private api: PessoasApi,
    private notificacaoService: NotificacaoService) { }

  ngOnInit(): void {
    this.iniciarForm();

    if (this.route.snapshot.params.id) {
      this.id = this.route.snapshot.params.id;
      this.preencherForm(this.id);
    }
    this.getTipo();
  }

  private getTipo() {

    this.pessoaForm.get('tipo').valueChanges.subscribe(tipo => {
      if (tipo == 'PJ') {
        this.isPessoaJuridica = true;
        this.isPessoaFisica = false;
        this.pessoaForm.get('sexo').clearValidators();
        this.pessoaForm.get('cpf').clearValidators();
        this.pessoaForm.get('data_nascimento').clearValidators();
      }

      if (tipo == 'PF') {
        this.isPessoaFisica = true;
        this.isPessoaJuridica = false;
        this.pessoaForm.get('cnpj').clearValidators();
        this.pessoaForm.get('razao_social').clearValidators();
      }
    });
  }

  private iniciarForm() {
    this.pessoaForm = this.form.group({
      tipo: ['', [Validators.required]],
      nome: ['', [Validators.required]],
      razao_social: ['', [Validators.required]],
      cpf: ['', [Validators.required]],
      cnpj: ['', [Validators.required]],
      sexo: ['', [Validators.required]],
      data_nascimento: [undefined, [Validators.required]],
      email: [''],
      telefone: [''],
      celular: [''],
      foto: [''],
      endereco: ['', [Validators.required]],
      numero: ['', [Validators.required]],
      complemento: [''],
      bairro: [''],
      cep: ['', Validators.required],
      cidade: ['', Validators.required],
      uf: ['', [Validators.required]]
    });
  }

  submit() {
    for (const i in this.pessoaForm.controls) {
      if (this.pessoaForm.controls.hasOwnProperty(i)) {


        this.pessoaForm.controls[i].markAsDirty();
        this.pessoaForm.controls[i].updateValueAndValidity();
      }
    }

    if (!this.pessoaForm.valid) {
      this.formSubmitted = true;
    } else {
      this.id ? this.api.editPessoa(this.id, this.pessoaForm.value).subscribe(response => {
        this.notificacaoService.showSuccess('success', Object.values(response), '')
      }) : this.api.savePessoa(this.pessoaForm.value).subscribe(response => {
        this.notificacaoService.showSuccess('success', Object.values(response), '')
      });
      this.voltarListagem();
    }
  }

  private preencherForm(id: any) {
    this.api.getById(id).subscribe((pessoa: any) => {
      this.id = pessoa.id_pessoa;
      this.pessoaForm.patchValue({
        tipo: pessoa.tipo,
        nome: pessoa.nome,
        razao_social: pessoa.razao_social,
        cpf: pessoa.cpf,
        cnpj: pessoa.cnpj,
        sexo: pessoa.sexo,
        data_nascimento: pessoa.data_nascimento,
        email: pessoa.email,
        telefone: pessoa.telefone,
        celular: pessoa.celular,
        foto: pessoa.foto,
        endereco: pessoa.endereco,
        numero: pessoa.numero,
        complemento: pessoa.complemento,
        bairro: pessoa.bairro,
        cep: pessoa.cep,
        cidade: pessoa.cidade,
        uf: pessoa.uf
      });
    });
  }

  public voltarListagem() {
    this.router.navigate(['/pessoas'])
  }

}
