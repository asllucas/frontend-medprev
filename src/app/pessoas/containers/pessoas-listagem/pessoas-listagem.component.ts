import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PessoasApi } from '../../api/pessoas.api';
import { Pessoas } from '../../models/pessoas';
import { MensagemService } from '../../services/mensagem.service';

@Component({
  selector: 'app-pessoas-listagem',
  templateUrl: './pessoas-listagem.component.html',
  styleUrls: ['./pessoas-listagem.component.scss']
})
export class PessoasListagemComponent implements OnInit {

  pessoas: Pessoas[] = [];
  isVisible = false;

  /**
   * Detalhes do Cliente
   */
  tipo: string;
  nome: string;
  razao_social: string;
  cpf: string;
  cnpj: string;
  sexo: string;
  data_nascimento: string;
  email: string;
  telefone: string;
  celular: string;
  foto: string;
  endereco: string;
  numero: string;
  complemento: string;
  bairro: string;
  cep: string;
  cidade: string;
  uf: string;

  constructor(private api: PessoasApi,
    private router: Router,
    private mensagemService: MensagemService) { }

  ngOnInit(): void {
    this.getPessoas();
  }

  private getPessoas() {
   this.api.getAllPessoas().subscribe((response: Pessoas[]) => {
     this.pessoas = response
   })
  }

  private getDetalhesPessoas(event: any) {
    this.api.getAllPessoas().subscribe(pessoa => {
      pessoa.map(res => {
        if (res.id_pessoa == event) {
          this.tipo = res.tipo;
          this.nome = res.nome;
          this.razao_social = res.razao_social;
          this.cpf = res.cpf;
          this.cnpj = res.cnpj;
          this.sexo = res.sexo;
          this.data_nascimento = res.data_nascimento;
          this.email = res.email;
          this.telefone = res.telefone;
          this.celular = res.celular;
          this.endereco = res.endereco;
          this.numero = res.numero;
          this.complemento = res.complemento;
          this.bairro = res.bairro;
          this.cep = res.cep;
          this.cidade = res.cidade;
          this.uf = res.uf;
        }
      });
    });
  }

  public async deletePessoa(id: number): Promise<void> {

    if (!await this.mensagemService.perguntaExclusao()) {
      return;
    }

    try {
      this.api.deletePessoa(id).subscribe(response => {
        this.getPessoas();
      });

    } catch (error) {
      console.error(error);
    }
  }

  showModal(event: any): void {
    this.isVisible = true;
    this.getDetalhesPessoas(event);
  }

  handleOk(): void {
    this.isVisible = false;
    this.voltarListagem();
  }

  handleCancel(): void {
    this.isVisible = false;
    this.voltarListagem();
  }

  public async acessarRotaCadastro() {
    this.router.navigate([`pessoas/cadastro`]);
  }

  public acessarRotaEdicao(id: number): void {
    this.router.navigate([`pessoas/${id}/alterar`]);
  }

  public voltarListagem() {
    this.router.navigate(['/pessoas'])
  }


}
