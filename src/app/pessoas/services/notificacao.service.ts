import { Injectable } from "@angular/core";
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Injectable({ providedIn: 'root' })
export class NotificacaoService {

    constructor(private notification: NzNotificationService) { }

    showSuccess(type, title, content) {
        this.notification.create(
            type,
            title,
            content
        );
    }
}